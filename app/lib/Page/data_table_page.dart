import 'package:flutter/material.dart';

class DataTablePage extends StatefulWidget {
  @override
  _DataTablePageState createState() => _DataTablePageState();
}

class _DataTablePageState extends State<DataTablePage> {
  @override
  Widget build(BuildContext context) {
    bool sort = true;

    return Container(
      child: Center(
        child: DataTable(
          sortColumnIndex: 1,
          sortAscending: true,
          columns: [
            DataColumn(label: Text('Name'), numeric: false),
            DataColumn(
                label: Text('Year'),
                onSort: (columnIndex, ascending) {
                  setState(() {
                    sort = !sort;
                  });
                },
                numeric: false),
            DataColumn(label: Text('Personal Pet'), numeric: true),
          ],
          rows: [
            DataRow(cells: [
              DataCell(Text('Enzo'), showEditIcon: true),
              DataCell(Text('1950')),
              DataCell(Text('Axolotl')),
            ]),
            DataRow(selected: true, cells: [
              DataCell(Text('Paul')),
              DataCell(Text('1980')),
              DataCell(Text('Bear')),
            ]),
            DataRow(cells: [
              DataCell(Text('Eric', style: TextStyle(color: Colors.blue))),
              DataCell(Text('1974', style: TextStyle(color: Colors.blue))),
              DataCell(Text('Shark', style: TextStyle(color: Colors.blue))),
            ]),
            DataRow(cells: [
              DataCell(Text('Fill name'), placeholder: true),
              DataCell(Text('1990')),
              DataCell(Text('Platypus')),
            ]),
          ],
        ),
      ),
    );
  }
}
