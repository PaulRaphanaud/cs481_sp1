import 'package:flutter/material.dart';

class ButtonsPage extends StatefulWidget {
  @override
  _ButtonsPageState createState() => _ButtonsPageState();
}

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }


class _ButtonsPageState extends State<ButtonsPage> {


  int selectedRadio;
  String dropdownValue = 'One';


  @override
  void initState() {
    super.initState();
    selectedRadio = 1;
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  onClick() {
    print('Button clicked');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextButton(
            onPressed: onClick,
            child: Text("Text Button", style: TextStyle(color: Colors.white)),
            style: ButtonStyle(
              elevation: MaterialStateProperty.all(8),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)
              )),
              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> states) {
                  if (states.contains(MaterialState.focused) ||
                      states.contains(MaterialState.pressed))
                    return Colors.greenAccent;
                  return Colors.redAccent; // Defer to the widget's default.
                },
              ),
            )
          ),
          ElevatedButton(onPressed: onClick, child: Text("Elevated Button")),
          OutlinedButton(onPressed: onClick, child: Text("Outlined Button")),
          Column(
            children: [
              Text("Floating Action Button", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              FloatingActionButton(onPressed: onClick, child: Icon(Icons.add)),
            ],
          ),
          Column(
            children: [
              Text("Icon Button", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              IconButton(onPressed: onClick, icon: Icon(Icons.volume_up)),
            ],
          ),
          Column(
            children: [
              Text("Popup Menu Button", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              PopupMenuButton<int>(
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Text("First"),
                  ),
                  PopupMenuItem(
                    value: 2,
                    child: Text("Second"),
                  ),
                ],
              ),
            ],
          ),
          Column(
            children: [
              Text("Drop Down Button", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              DropdownButton<String>(
                value: dropdownValue,
                icon: Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.deepPurple),
                underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                ),
                onChanged: (String newValue) {
                  print("DropDown $newValue");
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>['One', 'Two', 'Three', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ],
          ),
          Column(
            children: [
              Text("Radio Buttons", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget> [
                  Column(
                    children: [
                      Radio(
                        value: 1,
                        groupValue: selectedRadio,
                        activeColor: Colors.deepPurpleAccent,
                        onChanged: (val) {
                          print("Radio $val");
                          setSelectedRadio(val);
                        },
                      ),
                      Text('ON')
                    ],
                  ),
                  Column(
                    children: [
                      Radio(
                        value: 2,
                        groupValue: selectedRadio,
                        activeColor: Colors.deepPurpleAccent,
                        onChanged: (val) {
                          print("Radio $val");
                          setSelectedRadio(val);
                        },
                      ),
                      Text('OFF')
                    ],
                  ),
                ],
              ),
            ],
          ),
        ],
      )
    );
  }
}
