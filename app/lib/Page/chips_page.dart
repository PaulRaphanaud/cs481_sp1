import 'dart:ui';

import 'package:flutter/material.dart';

class ChipsPage extends StatefulWidget {
  bool isSelected = false;
  bool isActive = false;
  bool choice = false;

  @override
  _ChipsPageState createState() => _ChipsPageState();
}

class _ChipsPageState extends State<ChipsPage> {
  int selectedOption;
  int i;
  List<bool> _selected = [false, false, false];

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text(
              '\nInput Chips\n',
              softWrap: true,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            InputChip(
              avatar: CircleAvatar(
                backgroundColor: Colors.orangeAccent,
                child: Text('E'),
              ),
              label: Text('Eric Perez'),
              backgroundColor: Colors.white70,
              labelPadding: EdgeInsets.symmetric(horizontal: 6.0),
              elevation: 20,
              pressElevation: 5,
              onSelected: (isSelected) {
                setState(() {
                  widget.isSelected = !widget.isSelected;
                });
              },
              selected: widget.isSelected,
              selectedColor: Colors.lightBlue,
            ),
            Text(
              '\nChoice Chips\n',
              textAlign: TextAlign.left,
              softWrap: true,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              ChoiceChip(
                label: Text('Choice A'),
                padding: EdgeInsets.symmetric(horizontal: 10),
                backgroundColor: Colors.white70,
                elevation: 20,
                pressElevation: 5,
                selected: selectedOption == 1,
                onSelected: (bool selected) {
                  setState(() {
                    if (selected) {
                      selectedOption = 1;
                    }
                  });
                },
                selectedColor: Colors.orange,
              ),
              ChoiceChip(
                label: Text('Choice B'),
                padding: EdgeInsets.symmetric(horizontal: 10),
                backgroundColor: Colors.white70,
                elevation: 20,
                pressElevation: 5,
                selected: selectedOption == 2,
                onSelected: (bool selected) {
                  setState(() {
                    if (selected) {
                      selectedOption = 2;
                    }
                  });
                },
                selectedColor: Colors.orange,
              ),
              ChoiceChip(
                label: Text('Choice C'),
                padding: EdgeInsets.symmetric(horizontal: 10),
                backgroundColor: Colors.white70,
                elevation: 20,
                pressElevation: 5,
                selected: selectedOption == 3,
                onSelected: (bool selected) {
                  setState(() {
                    if (selected) {
                      selectedOption = 3;
                    }
                  });
                },
                selectedColor: Colors.orange,
              ),
            ]),
            Text(
              '\nFilter Chips\n',
              textAlign: TextAlign.left,
              softWrap: true,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FilterChip(
                  label: Text('Home Phone'),
                  backgroundColor: Colors.white70,
                  elevation: 20,
                  pressElevation: 5,
                  selected: _selected[0],
                  selectedColor: Colors.orange,
                  onSelected: (selected) {
                    setState(() {
                      _selected[0] = selected;
                    });
                  },
                ),
                FilterChip(
                  label: Text('Internet'),
                  backgroundColor: Colors.white70,
                  elevation: 20,
                  pressElevation: 5,
                  selected: _selected[1],
                  selectedColor: Colors.orange,
                  onSelected: (selected) {
                    setState(() {
                      _selected[1] = selected;
                    });
                  },
                ),
                FilterChip(
                  label: Text('Television'),
                  backgroundColor: Colors.white70,
                  elevation: 20,
                  pressElevation: 5,
                  selected: _selected[2],
                  selectedColor: Colors.orange,
                  onSelected: (selected) {
                    setState(() {
                      _selected[2] = selected;
                    });
                  },
                ),
              ],
            ),
            Text(
              '\nAction Chips\n',
              textAlign: TextAlign.left,
              softWrap: true,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            ActionChip(
                avatar: widget.isActive ? CircularProgressIndicator() : null,
                label: Text('${widget.isActive ? 'Active' : 'Activate'}'),
                backgroundColor: Colors.white70,
                elevation: 20,
                pressElevation: 5,
                onPressed: () {
                  setState(() {
                    widget.isActive = !widget.isActive;
                  });
                })
          ],
        ));
  }
}
