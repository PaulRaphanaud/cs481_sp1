import 'package:flutter/material.dart';
import 'buttons_page.dart';
import 'cards_page.dart';
import 'chips_page.dart';
import 'data_table_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController _controller;

  @override
  initState() {
    _controller = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        controller: _controller,
        children: [
          ButtonsPage(),
          CardsPage(),
          ChipsPage(),
          DataTablePage(),
        ],
      ),
    );
  }
}
