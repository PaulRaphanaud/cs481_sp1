import 'package:flutter/material.dart';

class CardsPage extends StatefulWidget {
  @override
  _CardsPageState createState() => _CardsPageState();
}

class _CardsPageState extends State<CardsPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            Card(
              borderOnForeground: true,
              margin: EdgeInsets.all(8),
              elevation: 0.5,
              shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.blueGrey,
              shadowColor: Colors.red,
              child: GestureDetector(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.album,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Praise feat. Gunna',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'Music by Tchami.',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          child: const Text(
                            'BUY TRACK',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                        FlatButton(
                          child: const Text(
                            'LISTEN',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Card(
              borderOnForeground: true,
              elevation: 0.5,
              shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.blueGrey,
              shadowColor: Colors.black,
              child: InkWell(
                onTap: () {},
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.album,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Wallifornia',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'Music by Honey&Badger.',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          child: const Text(
                            'BUY TRACK',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                        FlatButton(
                          child: const Text(
                            'LISTEN',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
